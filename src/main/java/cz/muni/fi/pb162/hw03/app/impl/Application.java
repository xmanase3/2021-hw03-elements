package cz.muni.fi.pb162.hw03.app.impl;

import com.beust.jcommander.Parameter;
import cz.muni.fi.pb162.hw03.app.cmd.CommandLine;

import java.io.IOException;


/**
 * Application CLI
 */
@SuppressWarnings("FieldMayBeFinal")
public final class Application {

    @Parameter(names = "--help", help = true)
    private boolean showUsage = false;

    /**
     * Application entry point
     *
     * @param args command line arguments of the application
     * @throws IOException on unrecoverable IO error
     */
    public static void main(String[] args) throws IOException {
        Application app = new Application();

        CommandLine cli = new CommandLine(app);
        cli.parseArguments(args);

        if (app.showUsage) {
            cli.showUsage();
        } else {
            app.run(cli);
        }
    }

    /**
     * Application runtime logic
     *
     * @param cli command line interface
     */
    private void run(CommandLine cli) {
        throw new UnsupportedOperationException("Implement this method!");
    }
}
